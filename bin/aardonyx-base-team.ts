#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AardonyxBaseTeamStack } from '../lib/aardonyx-base-team-stack';

const env: cdk.Environment = { account: '<ACCOUNT_ID>', region: 'us-west-1' };

const app = new cdk.App();
new AardonyxBaseTeamStack(app, 'AardonyxBaseTeamStack', { env });
