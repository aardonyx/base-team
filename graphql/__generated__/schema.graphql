# @model
type User {
  id: ID!
}


# @model
type Alpha {
  id: ID
  title: String
  description: String
  status: String
}


# @model
type Meal {
  _id: ObjectId!
  
  # translations: Translation!
  title: Translation!
  equipment: [MealEquipment!]!
  amounts: [MealAmounts!]!
  instructions: [Instruction!]!
  serve: [MealServe!]!
}

type MealAmounts {
  ingredient: Ingredient!
  amountPerPerson: Float!
}

type MealServe {
  timing: MealServeTiming!
  use: String!
}

type MealEquipment {
  equipment: Equipment!
  as: Alias
}


# # @model
type Ingredient {
  _id: ObjectId!
  name: Translation!
  priceInEurocents: Float
  unit: MeasurementUnit!
  home: Boolean
}

type Translation {
  en: String!
  nl: String!
}

type Instruction {
  id: String!
  order: Int
  type: InstructionType
  timerInSeconds: Float
  actions: [InstructionActions!]!
  ingredients: [InstructionIngredients]
  equipment: Equipment
  use: String
  as: Alias
}

type InstructionActions {
  timerAt: Float!
  action: Action!
}

type InstructionIngredients {
  order: Float!
  ingredient: Ingredient
  use: String
  amountDevided: Float!
}

enum MeasurementUnit {
  piece
  tableSpoon
  teaSpoon
  gram
  milliliter
  leaves
  clove
}

enum Action {
  
  # prepare actions
  rinse
  cutCubesSmall
  cutCubesNormal
  cutCubesLarge
  cutQuarts
  cutFine
  cutSlicesThin
  cutSlicesNormal
  cutSlicesThick
  rasp
  press
  tear
  heat_220_celcius
  bake
  grease
  
  # cook actions
  sprinkleBothSides
  frySoft
  fryNormal
  fryHard
  boil
  cook
  drain
  steamOut
  flip
  mix
  flavorPeperAndSalt
  flavorPeper
  flavorSalt
  layer
  layers
  layerLeft
  layerRight
}

enum Equipment {
  cutboard
  oven
  ovenDish
  fryingPan
  cookingPanWithCover
  bowlSmall
  bowlNormal1
  bowlNormal2
  bowlLarge
  plateNormal
  tableSpoon
  tableFork
}

enum Alias {
  pastaPan
  saladBowl
  sauceBowl
  dressing
  salad
  garnish
  sauce
  preparedOvenDish
}

enum InstructionType {
  cut
  prepare
  cook
}

enum MealServeTiming {
  before
  during
  after
}

type Query {
  _noop: String
  getMeal(id: ID!): Meal
  listMeals(filter: MealFilterInput, limit: Int, nextToken: String): MealConnection
  getUser(id: ID!): User
  listUsers(filter: UserFilterInput, limit: Int, nextToken: String): UserConnection
  getAlpha(id: ID!): Alpha
  listAlphas(filter: AlphaFilterInput, limit: Int, nextToken: String): AlphaConnection
}

type Mutation {
  _noop: String
  createMeal(input: MealCreateInput!, condition: MealConditionInput): Meal
  updateMeal(input: MealUpdateInput!, condition: MealConditionInput): Meal
  deleteMeal(input: MealDeleteInput!, condition: MealConditionInput): Meal
  createUser(input: UserCreateInput!, condition: UserConditionInput): User
  updateUser(input: UserUpdateInput!, condition: UserConditionInput): User
  deleteUser(input: UserDeleteInput!, condition: UserConditionInput): User
  createAlpha(input: AlphaCreateInput!, condition: AlphaConditionInput): Alpha
  updateAlpha(input: AlphaUpdateInput!, condition: AlphaConditionInput): Alpha
  deleteAlpha(input: AlphaDeleteInput!, condition: AlphaConditionInput): Alpha
}

type Subscription {
  _noop: String
  onCreateMeal: Meal @aws_subscribe(mutations: ["createMeal"])
  onUpdateMeal: Meal @aws_subscribe(mutations: ["updateMeal"])
  onDeleteMeal: Meal @aws_subscribe(mutations: ["deleteMeal"])
  onCreateUser: User @aws_subscribe(mutations: ["createUser"])
  onUpdateUser: User @aws_subscribe(mutations: ["updateUser"])
  onDeleteUser: User @aws_subscribe(mutations: ["deleteUser"])
  onCreateAlpha: Alpha @aws_subscribe(mutations: ["createAlpha"])
  onUpdateAlpha: Alpha @aws_subscribe(mutations: ["updateAlpha"])
  onDeleteAlpha: Alpha @aws_subscribe(mutations: ["deleteAlpha"])
}

scalar AWSDateTime

scalar AWSDate

scalar AWSURL

directive @aws_subscribe(mutations: [String]!) on FIELD_DEFINITION

type S3Object {
  bucket: String!
  region: String!
  key: String!
}

scalar ObjectId

enum AttributeTypes {
  binary
  binarySet
  bool
  list
  map
  number
  numberSet
  string
  stringSet
  _null
}

enum SortDirection {
  ASC
  DESC
}

input StringInput {
  ne: String
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  contains: String
  notContains: String
  between: [String]
  beginsWith: String
  attributeExists: Boolean
  attributeType: AttributeTypes
  size: SizeInput
}

input IDInput {
  ne: ID
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  contains: ID
  notContains: ID
  between: [ID]
  beginsWith: ID
  attributeExists: Boolean
  attributeType: AttributeTypes
  size: SizeInput
}

input IntInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
  attributeExists: Boolean
  attributeType: AttributeTypes
}

input FloatInput {
  ne: Float
  eq: Float
  le: Float
  lt: Float
  ge: Float
  gt: Float
  between: [Float]
  attributeExists: Boolean
  attributeType: AttributeTypes
}

input BooleanInput {
  ne: Boolean
  eq: Boolean
  attributeExists: Boolean
  attributeType: AttributeTypes
}

input SizeInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
}

input StringKeyConditionInput {
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  between: [String]
  beginsWith: String
}

input IDKeyConditionInput {
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  between: [ID]
  beginsWith: ID
}

input IntKeyConditionInput {
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
}


# #
# # E N U M S
# #
enum LambdaResult {
  success
  error
}

input MealFilterInput {
  
  # fields
  _id: IDInput
  
  # title: Translation
  # equipment: [MealEquipment]
  # amounts: [MealAmounts]
  # miseEnPlace: [Instruction]
  # prepare: [Instruction]
  # cook: [Instruction]
  # serve: [MealServe]
  # default operators
  and: [MealFilterInput]
  or: [MealFilterInput]
  not: MealFilterInput
}

input MealConditionInput {
  
  # fields
  _id: IDInput
  
  # title: Translation
  # equipment: [MealEquipment]
  # amounts: [MealAmounts]
  # miseEnPlace: [Instruction]
  # prepare: [Instruction]
  # cook: [Instruction]
  # serve: [MealServe]
  # default operators
  and: [MealConditionInput]
  or: [MealConditionInput]
  not: MealConditionInput
}

input MealCreateInput {
  
  # fields
  _id: IDInput
}

input MealUpdateInput {
  
  # fields
  _id: IDInput
}

input MealDeleteInput {
  _id: IDInput
}

type MealConnection {
  items: [Meal]
  nextToken: String
}

input UserFilterInput {
  
  # fields
  id: IDInput
  name: StringInput
  email: StringInput
  
  # default operators
  and: [UserFilterInput]
  or: [UserFilterInput]
  not: UserFilterInput
}

input UserConditionInput {
  
  # fields
  title: StringInput
  name: StringInput
  email: StringInput
  
  # default operators
  and: [UserConditionInput]
  or: [UserConditionInput]
  not: UserConditionInput
}

input UserCreateInput {
  
  # fields
  id: ID
  name: String!
  email: StringInput
}

input UserUpdateInput {
  
  # fields
  id: ID!
  name: String
  email: StringInput
}

input UserDeleteInput {
  id: ID
}

type UserConnection {
  items: [User]
  nextToken: String
}

type AlphaConnection {
  items: [Alpha]
  nextToken: String
}

input AlphaFilterInput {
  
  # fields (only non-connection and incl. ID)
  id: IDInput
  title: StringInput
  description: StringInput
  status: StringInput
  
  # default operators
  and: [AlphaFilterInput]
  or: [AlphaFilterInput]
  not: AlphaFilterInput
}

input AlphaConditionInput {
  
  # fields (only non-connection and without ID)
  title: StringInput
  description: StringInput
  status: StringInput
  
  # default operators
  and: [AlphaConditionInput]
  or: [AlphaConditionInput]
  not: AlphaConditionInput
}

input AlphaCreateInput {
  
  # fields (only non-connection and incl. ID)
  id: ID
  title: String!
  description: String
  status: String
}

input AlphaUpdateInput {
  
  # fields (only non-connection and incl. ID)
  id: ID!
  title: String
  description: String
  status: String
}

input AlphaDeleteInput {
  id: ID
}

schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}
