import { LambdaPayloadApiResolver, LambdaResult, TypeHandler, LambdaResultStatus } from '../../types/lambda';
import { generate } from './lib/generate-voice';

export const handler = async (payload: LambdaPayloadApiResolver): Promise<LambdaResult> => {
    const typeHandler = resolvers[payload.typeName];
    if (typeHandler) {
        const resolver = typeHandler[payload.fieldName];
        if (resolver) {
            return await resolver(payload);
        }
    }
    throw new Error('Resolver not found.');
};

const resolvers: Record<string, TypeHandler | undefined> = {
    Mutation: {
        async generate(): Promise<LambdaResult> {
            generate();
            return { status: LambdaResultStatus.SUCCESS };
        },
    },
};
