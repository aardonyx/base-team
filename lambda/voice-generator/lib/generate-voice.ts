import meal0001 from '../../../mocks/db-data/meal/0001';
import { Meal } from './meal';
import { HumanText } from './human-text';
import { Language } from './translations';

const LANG = Language.en;

export const generate = () => {

    const meal = new Meal(meal0001, LANG);
    const humanText = new HumanText(meal, LANG);

    let text = humanText.start();
    text += humanText.miseEnPlace();
    text += humanText.prepare();
    text += humanText.cook();
    text += humanText.end();

    console.log(text);
};
