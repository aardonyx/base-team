import meal0001 from "../../../mocks/db-data/meal/0001";
import { Language } from "./translations";
import { HumanText } from "./human-text";
import { Meal } from "./meal";

const LANG = Language.en;

const meal = new Meal(meal0001, LANG);
const humanText = new HumanText(meal, LANG);

describe('HumanText', () => {
    describe('mise-en-place', () => {
        it('does not print out the equipment', () => {
            const text = humanText.miseEnPlace();
            console.log(text)
            expect(text).toContain('<break time="1s"/> Let\'s first chop some ingredients <break time="1s"/>');
            expect(text).toContain('cut in slices the eggplant <break time="0.3s"/>.');
            expect(text).toContain('and the tomato <break time="0.3s"/>.');
            expect(text).toContain('press the garlic <break time="0.3s"/>.');
            // expect(text).toContain('rasp the old cheese <break time=\"0.3s\"/>.');
            expect(text).toContain('for garnish <break time="0.3s"/> tear the basil <break time="0.3s"/>.');
            expect(text).toContain('<break time="5s"/>Repeat this section?')
        });
    });
    describe('prepare', () => {
        it('prints equipment, action, ingredient, final action', () => {
            const text = humanText.prepare();
            console.log(text)
            
            expect(text).toContain('<break time=\"1s\"/> Before we start cooking <break time=\"1s\"/>');
            expect(text).toContain('use the oven <break time=\"0.3s\"/> and heat until 220 centigrade <break time=\"5s\"/>.');
            expect(text).toContain('use the oven dish <break time="0.3s"/> and grease with a half of the olive oil <break time="5s"/>.');
            expect(text).toContain('use the pastapan <break time="0.3s"/> and boil with the water <break time="5s"/>.');
            expect(text).toContain('sprinkle both sides of the eggplant with a half of the olive oil <break time="5s"/>.');
        });
    });
    /*
    The Layers sections should be handled differently.
    add layers to contents of the oven dish with <break time="0.3s"/> 1/3 of the sauce, half of the eggplant and half of the tomato <break time="1s"/> then again 1/3 of the sauce, half of the eggplant and half of the tomato <break time="1s"/> <break time="0.3s"/> and finally <break time="0.3s"/> the rest of the sauce  and the rasped old cheese <break time="5s"/>.
    */
    describe('cook', () => {
        it('prints equipment, action, ingredient, timer, halfway, final action', () => {
            const text = humanText.cook();
            console.log(text)
            expect(text).toContain('<break time=\"1s\"/> Now let\'s start cooking <break time=\"1s\"/>')
            expect(text).toContain('for dressing <break time=\"0.3s\"/> use the salad bowl <break time=\"0.3s\"/> and mix with the olive oil extra virgin and the balsamic vinegar white <break time=\"0.3s\"/> and finally <break time=\"0.3s\"/> season with peper and salt <break time=\"5s\"/>.');
            expect(text).toContain('for salad <break time=\"0.3s\"/> mix dressing with the corn salad and the arugula <break time=\"5s\"/>.');
            expect(text).toContain('fry the eggplant for 10 minutes <break time=\"0.3s\"/> and halfway <break time="0.3s"/> flip <break time="0.3s"/> and finally <break time="0.3s"/> season with peper and salt <break time="5s"/>.');
            expect(text).toContain('cook contents of the pastapan for 14 minutes with the penne <break time="0.3s"/> and finally <break time="0.3s"/> drain and steam out <break time="5s"/>.');
            expect(text).toContain('for sauce <break time="0.3s"/> mix the garlic with the tomato passata and the balsamic vinegar black and the sugar <break time="0.3s"/> and finally <break time="0.3s"/> season with peper and salt <break time="5s"/>.');
            //expect(text).toContain('add layers to contents of the oven dish with <break time="0.3s"/> 0.3 sauce and <break time="0.3s"/> a half of contents of the frying pan and <break time="0.3s"/> a half of tomato and <break time="0.3s"/> 0.3 sauce and <break time="0.3s"/> a half of contents of the frying pan and <break time="0.3s"/> a half of tomato and <break time="0.3s"/> 0.4 sauce and <break time="0.3s"/> old cheese <break time="2s"/>.');
        });
    });
});
