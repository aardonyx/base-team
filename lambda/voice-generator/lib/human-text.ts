import { Action, Instruction, InstructionType, Meal as GraphQLMeal } from "../../../types/__generated__/graphql";
import { Meal } from "./meal";
import { Language, NL, EN } from './translations';

export const PAUSE = '<break time="1s"/>';
export const PAUSE_SHORT = '<break time="0.3s"/>';
export const PAUSE_NORMAL = '<break time="2s"/>';
export const PAUSE_LONG = '<break time="5s"/>';

let lastActionPerformed: Action;

export class HumanText {
    private _lang: Language;
    private _translation: typeof EN;

    private _meal: Meal<GraphQLMeal>;

    constructor(meal: Meal<GraphQLMeal>, lang: Language){
        this._lang = lang;
        this._translation = EN;
        if (lang === 'nl') {
            this._translation = NL;
        }
        this._meal = meal;
    }

    public start(): string {

        return `<speak>\n${this._translation.directions.START_MEAL(this._meal.title)} ${PAUSE}\n`
    }

    public end(): string {

        return `${this._translation.directions.END_MEAL()}\n</speak>`;
    }

    public miseEnPlace(): string {
        let text = `${PAUSE} ${this._translation.directions.START_MISEENPLACE()} ${PAUSE}\n`;
        this._meal.graphQLmeal.instructions
            .filter((instruction) => instruction.type === InstructionType.cut)
            // so that the same action can be combined (cut eggplant and tomato)
            .sort((a, b) => a.actions[0].action.localeCompare(b.actions[0].action))
            .forEach((instruction) => text +=  this._instructionText(instruction));
        text += `${PAUSE_LONG}${this._translation.directions.END_SECTION()}\n`;

        return text;
    }

    public prepare(): string {
        let text = `${PAUSE} ${this._translation.directions.START_PREPARE()} ${PAUSE}\n`;
        this._meal.graphQLmeal.instructions
            .filter((instruction) => instruction.type === InstructionType.prepare)
            .forEach((instruction) => text +=  this._instructionText(instruction));

        return text;
    }

    public cook(): string {
        let text = `${PAUSE} ${this._translation.directions.START_COOKING()} ${PAUSE}\n`
        this._meal.graphQLmeal.instructions
            .filter((instruction) => instruction.type === InstructionType.cook)
            .forEach((instruction) => text += this._instructionText(instruction));

        return text;
    }

    private _instructionText = (instruction: Instruction): string => {

        let text = '';

        // start with saying what this step will create
        if (instruction.as) {
            text += this._translation.words.for + ' ';
            text += this._translation.alias[instruction.as] + ' ' + PAUSE_SHORT + ' ';
        }
    
        // if not referencing to previous step (via use), then just mention the equipment to use
        if (!instruction.use && instruction.type !== InstructionType.cut) {
            text += this._translation.words.use + ' ';
            text += this._meal.resolveEquipmentName(instruction!.equipment!) + ' ' + PAUSE_SHORT + ' ';
            text += this._translation.words.and + ' ';
        }
    
        // mention the actions before the ingredient
        instruction.actions.forEach((action, index) => {
            if (action.timerAt !== 0) return;
            if (lastActionPerformed === action.action) {
                text += this._translation.words.and + ' ';
            } else {
                text += (index !== 0 ? this._translation.words.and  + ' ' : '');
                text += this._translation.actions[action.action] + ' ';
            }
            lastActionPerformed = action.action;
        });
    
        // reference first what is used with the ingredients
        if (instruction.use) {
            const foundUse = this._meal.findUse(instruction.use);
            text += this._meal.textFromFoundUse(foundUse);
        }

        // add timer when needed
        if (instruction.timerInSeconds) {
            text += this._translation.directions.TIMER(instruction.timerInSeconds) + ' ';
        }
    
        // when ingredients are being mentioned
        if (instruction.ingredients && instruction.ingredients.length > 0) {
    
            if (instruction.type === InstructionType.prepare || instruction.type === InstructionType.cook) {
                text += this._translation.words.with + ' ';
            }
    
            instruction.ingredients.forEach((ingredient, index) => {

                text += (index !== 0 ? this._translation.words.and + ' ' : '');
    
                if (ingredient && ingredient.amountDevided !== 1) {
                    text += this._meal.calcAmountIngredient(ingredient!) + ' ';
                }

                // for a regular ingredient
                if (ingredient && ingredient.ingredient) {
                    // TODO: lang should be handled by new Meal();
                    text += this._translation.words.the + ' ' + ingredient!.ingredient.name[this._lang] + ' ';
                }

                // for a referenced alias
                if (ingredient && ingredient.use) {
                    const foundUse = this._meal.findUse(ingredient.use);
                    text += this._meal.textFromFoundUse(foundUse);
                }

            });
        }
    
        // mention the actions after the ingredient
        let finallyHad = false;
        instruction.actions.forEach((action, index) => {
            if (action.timerAt === 0) return;
            if (lastActionPerformed === action.action) {
                text += this._translation.words.and + ' ';
            } else {

                if (action.timerAt === 0.5) {
                    text += `${PAUSE_SHORT} ${this._translation.words.and} ${this._translation.words.halfway} ${PAUSE_SHORT} `;
                } else if (action.timerAt === 1 && !finallyHad) {
                    text += `${PAUSE_SHORT} ${this._translation.words.and} ${this._translation.words.finally} ${PAUSE_SHORT} `;
                    finallyHad = true;
                } else if (index !== 0) {
                    text += `${this._translation.words.and} `;
                }

                text += this._translation.actions[action.action] + ' ';
            }
            lastActionPerformed = action.action;
        });

        if (instruction.type === InstructionType.cut) {
            text += PAUSE_SHORT;
        }
        if (instruction.type === InstructionType.prepare) {
            text += PAUSE_LONG;
        }
        if (instruction.type === InstructionType.cook) {
            text += PAUSE_LONG;
        }
    
        return text + '.\n';
    }
}
