import { Equipment, Instruction, InstructionIngredients, Meal as GraphQLMeal , MeasurementUnit} from '../../../types/__generated__/graphql';
import { Language, EN, NL } from './translations';

export class Meal<T extends GraphQLMeal> {
    private _lang: Language;
    private _translation: typeof EN;

    public graphQLmeal: T;
    public title: string;

    constructor(meal: T, lang: Language) {
        this._lang = lang;
        this._translation = EN;
        if (lang === 'nl') {
            this._translation = NL;
        }

        this.graphQLmeal = meal;
        this.title = meal.title[lang];
    }

    public resolveEquipmentName(inputEquipment: Equipment): string {
        const foundEquipment = this.graphQLmeal.equipment.find((equipment) => equipment.equipment === inputEquipment)
        
        if (foundEquipment && foundEquipment.as) {
            return this._translation.words.the + ' ' + this._translation.alias[foundEquipment.as];
        }
    
        return this._translation.words.the + ' ' + this._translation.equipment[inputEquipment];
    }

    public findUse(use: string): Instruction {
        let foundUse: Instruction | undefined;
    
        foundUse = this.graphQLmeal.instructions
            .find((instruction) => instruction.id === use);
    
        // circular (if it references another use; but if it has equipment, we know enough)
        if (foundUse && foundUse.use && !foundUse.equipment) {
            foundUse = this.findUse(foundUse.use);
        }

        if (!foundUse) throw new Error('use could not be resolved to an alias or ingredient')

        return foundUse;
    }

    public textFromFoundUse(foundUse: Instruction): string {
        let text = '';

        if (foundUse && foundUse.equipment && !foundUse.as) {
            text += this._translation.words.content + ' ';
            text += this.resolveEquipmentName(foundUse.equipment) + ' ';
        }

        if (foundUse && foundUse.as) {
            text += this._translation.alias[foundUse.as] + ' ';
        }

        if (foundUse && foundUse.ingredients && !foundUse.equipment && !foundUse.as) {
            // TODO: lang should already have been handled with new Meal();
            text += foundUse.ingredients.map((ingredient) => this._translation.words.the + ' ' + ingredient!.ingredient!.name![this._lang] + ' ');
        }

        return text;
    }

    public calcAmountIngredient(inputIngredient: InstructionIngredients): string {

        if (!inputIngredient.amountDevided) return '';

        if (!inputIngredient.ingredient) {
            return inputIngredient.amountDevided === 0.5 ? this._translation.words.half : inputIngredient.amountDevided.toString();
        }
    
        const name = inputIngredient.ingredient!.name[this._lang];
        const devided = inputIngredient.amountDevided.valueOf();
        const ingre = this.graphQLmeal.amounts.find(ingredient => ingredient.ingredient!.name[this._lang] === name);
        
        if (!ingre) throw new Error('Ingredient not found in list of meal.amounts');
        
        const amount = ingre.amountPerPerson * devided;
    
        return amount === 0.5 ? this._translation.words.half : amount + ' ' + this._translation.measurementUnits[ingre!.ingredient.unit];
    };
    
}