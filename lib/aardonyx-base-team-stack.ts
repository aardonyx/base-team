import * as cdk from '@aws-cdk/core';
import { authConfig } from '../stack-config';
import { Auth } from '../constructs/auth';
import { apiConfig } from "../stack-config";
import { Api } from "../constructs/api";

export class AardonyxBaseTeamStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const auth = new Auth(this, 'Auth', authConfig);

    new Api(this, "Api", {
      apiName: apiConfig.apiName,
      userPool: auth.userPool,
      // functions: lambdas.functions,
    });

  }
}
