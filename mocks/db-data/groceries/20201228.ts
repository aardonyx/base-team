import { getObjectId } from "mongo-seeding";

// these are ingredients from meal0001, excluding the ones with home=true
export = {
    user: getObjectId('user0001'),
    delivery: 'odin',
    ingredients: [
        {
            ingredient: getObjectId('rasped_old_cheese'), 
            amountPerPerson: 40,
            shopUrl: 'https://www.odinbezorgdienst.nl/pittig-geraspte-kaas.html',
            priceInCentsEUR: 199,
            unitPerProduct: 100,
            unitsNeeded: 80,
            orderAmount: 0.8
        },
        {
            ingredient: getObjectId('eggplant'),
            amountPerPerson: 0.5,
            shopUrl: 'https://www.odinbezorgdienst.nl/aubergine.html',
            priceInCentsEUR: 92,
            unitPerProduct: 1,
            unitsNeeded: 1,
            orderAmount: 1
        },
        {
            ingredient: getObjectId('tomato'),
            amountPerPerson: 1,
            shopUrl: 'https://www.odinbezorgdienst.nl/ronde-tomaten.html',
            priceInCentsEUR: 262,
            unitPerProduct: 5, // 750gr for product with 150gr per piece = 5 units
            unitsNeeded: 2,
            orderAmount: 0.4
        },
        {
            ingredient: getObjectId('garlic,'),
            amountPerPerson: 1,
            shopUrl: 'https://www.odinbezorgdienst.nl/knoflook-bolletjes.html',
            priceInCentsEUR: 129,
            unitPerProduct: 24, // 8 cloves times 3 pieces (from shop) is 24 units
            unitsNeeded: 2, // 1 amountPerPerson times 2 amountOfPersons
            orderAmount: 0.08 // 2 units needed divided by 24 units from one product
        },
        {
            ingredient: getObjectId('penne'),
            amountPerPerson: 90,
            shopUrl: 'https://www.odinbezorgdienst.nl/penne-bloem.html',
            priceInCentsEUR: 171,
            unitPerProduct: 500,
            unitsNeeded: 180,
            orderAmount: 0.36
        },
        {
            ingredient: getObjectId('tomato_passata'),
            amountPerPerson: 150,
            shopUrl: 'https://www.odinbezorgdienst.nl/passata-gezeefde-tomaten.html',
            priceInCentsEUR: 112,
            unitPerProduct: 500,
            unitsNeeded: 300,
            orderAmount: 0.6
        },
        {
            ingredient: getObjectId('corn_salad'),
            amountPerPerson: 10,
            shopUrl: 'https://www.odinbezorgdienst.nl/veldsla.html',
            priceInCentsEUR: 199,
            unitPerProduct: 75,
            unitsNeeded: 20,
            orderAmount: 0.26
        },
        {
            ingredient: getObjectId('rucola'),
            amountPerPerson: 10,
            shopUrl: 'https://www.odinbezorgdienst.nl/rucola-6809.html',
            priceInCentsEUR: 169,
            unitPerProduct: 75,
            unitsNeeded: 20,
            orderAmount: 0.26
        }
    ]
}
