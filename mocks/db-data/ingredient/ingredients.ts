import { getObjectId } from 'mongo-seeding';
import { 
    Ingredient,
    MeasurementUnit
} from '../../../types/__generated__/graphql';

export const eggplant: Ingredient = {
    _id: getObjectId('eggplant'),
    name: {
        en: 'eggplant',
        nl: 'aubergine',
    },
    unit: MeasurementUnit.piece,
}

export const tomato: Ingredient = {
    _id: getObjectId('tomato'),
    name: {
        en: 'tomato',
        nl: 'tomaat',
    },
    unit: MeasurementUnit.piece,
}

export const garlic: Ingredient = {
    _id: getObjectId('garlic'),
    name: {
        en: 'garlic',
        nl: 'knoflook',
    },
    unit: MeasurementUnit.clove,
}

export const tomato_passata: Ingredient = {
    _id: getObjectId('tomato_passata'),
    name: {
        en: 'tomato passata',
        nl: 'tomatenpassata',
    },
    unit: MeasurementUnit.gram,
}

export const rasped_old_cheese: Ingredient = {
    _id: getObjectId('rasped_old_cheese'),
    name: {
        en: 'rasped old cheese',
        nl: 'geraspte oude kaas',
    },
    unit: MeasurementUnit.gram,
}

export const rigatoni: Ingredient = {
    _id: getObjectId('rigatoni'),
    name: {
        en: 'rigatoni',
        nl: 'rigatoni',
    },
    unit: MeasurementUnit.gram,
}

export const penne: Ingredient = {
    _id: getObjectId('penne'),
    name: {
        en: 'penne',
        nl: 'penne'
    },
    unit: MeasurementUnit.gram
}

export const corn_salad: Ingredient = {
    _id: getObjectId('corn_salad'),
    name: {
        en: 'corn salad',
        nl: 'veldsla',
    },
    unit: MeasurementUnit.gram,
}

export const rucola: Ingredient = {
    _id: getObjectId('rucola'),
    name: {
        en: 'arugula',
        nl: 'rucola',
    },
    unit: MeasurementUnit.gram,
}

export const basil: Ingredient = {
    _id: getObjectId('basil'),
    name: {
        en: 'basil',
        nl: 'basilicum',
    },
    unit: MeasurementUnit.leaves,
    home: true
}

export const olive_oil: Ingredient = {
    _id: getObjectId('olive_oil'),
    name: {
        en: 'olive oil',
        nl: 'olijfolie',
    },
    unit: MeasurementUnit.tableSpoon,
    home: true
}

export const olive_oil_extra_virgin: Ingredient = {
    _id: getObjectId('olive_oil_extra_virgin'),
    name: {
        en: 'olive oil extra virgin',
        nl: 'olijfolie extra vierge',
    },
    unit: MeasurementUnit.tableSpoon,
    home: true
}

export const balsamic_vinegar_black: Ingredient = {
    _id: getObjectId('balsamic_vinegar_black'),
    name: {
        en: 'balsamic vinegar black',
        nl: 'balsamico azijn zwart',
    },
    unit: MeasurementUnit.tableSpoon,
    home: true
}

export const balsamic_vinegar_white: Ingredient = {
    _id: getObjectId('balsamic_vinegar_white'),
    name: {
        en: 'balsamic vinegar white',
        nl: 'balsamico azijn wit',
    },
    unit: MeasurementUnit.teaSpoon,
    home: true
}

export const sugar: Ingredient = {
    _id: getObjectId('sugar'),
    name: {
        en: 'sugar',
        nl: 'suiker',
    },
    unit: MeasurementUnit.teaSpoon,
    home: true
}

export const water: Ingredient = {
    _id: getObjectId('water'),
    name: {
        en: 'water',
        nl: 'water',
    },
    unit: MeasurementUnit.milliliter,
    home: true
}

export const onion_yellow: Ingredient = {
    _id: getObjectId('onion_yellow'),
    name: {
        en: 'yellow onion',
        nl: 'gele ui',
    },
    unit: MeasurementUnit.piece,
}

export const stock_cube_vegetable: Ingredient = {
    _id: getObjectId('stock_cube_vegetable'),
    name: {
        en: 'vegetable stock cube',
        nl: 'groentebouillonblokje',
    },
    unit: MeasurementUnit.piece,
    home: true
}

export const pepper_bell_yellow: Ingredient = {
    _id: getObjectId('pepper_bell_yellow'),
    name: {
        en: 'yellow bell pepper',
        nl: 'gele paprika',
    },
    unit: MeasurementUnit.piece,
}

export const pepper_red: Ingredient = {
    _id: getObjectId('pepper_red'),
    name: {
        en: 'red pepper',
        nl: 'rode peper',
    },
    unit: MeasurementUnit.piece,
}

export const parsley: Ingredient = {
    _id: getObjectId('parsley'),
    name: {
        en: 'parsley',
        nl: 'peterselie',
    },
    unit: MeasurementUnit.piece,
    home: true
}

export const egg: Ingredient = {
    _id: getObjectId('egg'),
    name: {
        en: 'egg',
        nl: 'ei',
    },
    unit: MeasurementUnit.piece,
    home: true
}

export const tomato_plum: Ingredient = {
    _id: getObjectId('tomato_plum'),
    name: {
        en: 'plum tomato',
        nl: 'pruimtomaat',
    },
    unit: MeasurementUnit.piece,
}

export const goat_cheese: Ingredient = {
    _id: getObjectId('goat_cheese'),
    name: {
        en: 'goat cheese',
        nl: 'geitenkaas',
    },
    unit: MeasurementUnit.gram,
}

export const bun_beer_bostel: Ingredient = {
    _id: getObjectId('bun_beer_bostel'),
    name: {
        en: 'beer bostel bun',
        nl: 'bierbostelbroodje',
    },
    unit: MeasurementUnit.piece,
}
