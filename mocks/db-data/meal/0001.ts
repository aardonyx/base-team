import {
    balsamic_vinegar_black,
    balsamic_vinegar_white,
    basil,
    corn_salad,
    eggplant,
    garlic,
    rasped_old_cheese,
    olive_oil,
    olive_oil_extra_virgin,
    penne,
    rucola,
    sugar,
    tomato,
    tomato_passata,
    water,
} from '../ingredient/ingredients';

import {
    Action,
    Alias,
    Equipment,
    InstructionType,
    Meal,
    MealServeTiming
} from '../../../types/__generated__/graphql';

import { getObjectId } from 'mongo-seeding';

// if an id like 'mep', 'prep' or 'cook' is referenced somewhere else with 'use',
// then that object should also have an 'as' to make a 'beautiful reference'.

export = {
    _id: getObjectId('meal0001'),
    title: {
        en: 'Ovendish with organic eggplant and penne',
        nl: 'Ovengerecht met biologische aubergine en penne'
    },
    equipment: [
        { equipment: Equipment.cutboard },
        { equipment: Equipment.oven },
        { equipment: Equipment.ovenDish },
        { equipment: Equipment.cookingPanWithCover, as: Alias.pastaPan },
        { equipment: Equipment.bowlNormal1, as: Alias.saladBowl },
        { equipment: Equipment.fryingPan },
        { equipment: Equipment.bowlNormal2, as: Alias.sauceBowl },
        { equipment: Equipment.plateNormal },
        { equipment: Equipment.tableSpoon },
        { equipment: Equipment.tableFork }
    ],
    amounts: [
        // make sure each ingredient used, is also in this list.
        { ingredient: balsamic_vinegar_black, amountPerPerson: 0.5 },
        { ingredient: balsamic_vinegar_white, amountPerPerson: 0.5 },
        { ingredient: basil, amountPerPerson: 3 },
        { ingredient: corn_salad, amountPerPerson: 10 },
        { ingredient: eggplant, amountPerPerson: 0.5 },
        { ingredient: garlic, amountPerPerson: 1 },
        { ingredient: olive_oil, amountPerPerson: 1 },
        { ingredient: olive_oil_extra_virgin, amountPerPerson: 0.5 },
        { ingredient: penne, amountPerPerson: 90 },
        { ingredient: rasped_old_cheese, amountPerPerson: 40 },
        { ingredient: rucola, amountPerPerson: 10 },
        { ingredient: sugar, amountPerPerson: 1 },
        { ingredient: tomato, amountPerPerson: 1 },
        { ingredient: tomato_passata, amountPerPerson: 150 },
        { ingredient: water, amountPerPerson: 250 },
    ],
    instructions: [
        {
            id: 'mep2',
            type: InstructionType.cut,
            actions: [ { timerAt: 0, action: Action.cutSlicesNormal } ],
            ingredients:[ {order: 1, ingredient: eggplant, amountDevided: 1 } ],
        },
        {
            id: 'mep4',
            type: InstructionType.cut,
            actions: [ { timerAt: 0, action: Action.press } ],
            ingredients: [ { order: 1, ingredient: garlic, amountDevided: 1 } ],
        },
        {
            id: 'mep3',
            type: InstructionType.cut,
            actions: [ { timerAt: 0, action: Action.cutSlicesNormal } ],
            ingredients: [ { order: 1, ingredient: tomato, amountDevided: 1 } ],
        },
        {
            id: 'mep5',
            type: InstructionType.cut,
            actions: [ { timerAt: 0, action: Action.tear } ],
            ingredients: [ { order: 1, ingredient: basil, amountDevided: 1 } ],
            as: Alias.garnish
        },
        {
            id: 'prep1',
            type: InstructionType.prepare,
            actions: [ { timerAt: 0, action: Action.heat_220_celcius } ],
            ingredients: [],
            equipment: Equipment.oven
        },
        {
            id: 'prep2',
            type: InstructionType.prepare,
            actions: [ { timerAt: 0, action: Action.grease } ],
            ingredients: [ { order: 1, ingredient: olive_oil, amountDevided: 0.5 } ],
            equipment: Equipment.ovenDish,
        },
        {
            id: 'prep3',
            type: InstructionType.prepare,
            actions: [ { timerAt: 0, action: Action.boil } ],
            ingredients: [ { order: 1, ingredient: water, amountDevided: 1 } ],
            equipment: Equipment.cookingPanWithCover,
        },
        {
            id: 'prep4',
            type: InstructionType.prepare,
            actions: [ { timerAt: 0, action: Action.sprinkleBothSides } ],
            ingredients: [ { order: 1, ingredient: olive_oil, amountDevided: 0.5 } ],
            use: 'mep2'
        },
        {
            id: 'cook1',
            type: InstructionType.cook,
            actions: [
                { timerAt: 0, action: Action.mix },
                { timerAt: 1, action: Action.flavorPeperAndSalt }
            ],
            ingredients: [
                { order: 1, ingredient: olive_oil_extra_virgin, amountDevided: 1 },
                { order: 2, ingredient: balsamic_vinegar_white, amountDevided: 1 }
            ],
            equipment: Equipment.bowlNormal1,
            as: Alias.dressing
        },
        {
            id: 'cook2',
            type: InstructionType.cook,
            actions: [ { timerAt: 0, action: Action.mix } ],
            ingredients: [
                { order: 1, ingredient: corn_salad, amountDevided: 1 },
                { order: 2, ingredient: rucola, amountDevided: 1 }
            ],
            use: 'cook1',
            as: Alias.salad
        },
        {
            id: 'cook3',
            type: InstructionType.cook,
            timerInSeconds: 10 * 60,
            actions: [
                { timerAt: 0, action: Action.fryNormal },
                { timerAt: 0.5, action: Action.flip },
                { timerAt: 1, action: Action.flavorPeperAndSalt }
            ],
            ingredients: [],
            use: 'prep4',
            equipment: Equipment.fryingPan,
        },
        {
            id: 'cook4',
            type: InstructionType.cook,
            timerInSeconds: 14 * 60,
            actions: [
                { timerAt: 0, action: Action.cook },
                { timerAt: 1, action: Action.drain },
                { timerAt: 1, action: Action.steamOut }
            ],
            ingredients: [ { order: 1, ingredient: penne, amountDevided: 1 } ],
            use: 'prep3',
        },
        {
            id: 'cook6',
            type: InstructionType.cook,
            actions: [
                { timerAt: 0, action: Action.mix },
                { timerAt: 1, action: Action.flavorPeperAndSalt }
            ],
            ingredients: [
                { order: 1, ingredient: tomato_passata, amountDevided: 1 },
                { order: 2, ingredient: balsamic_vinegar_black, amountDevided: 1 },
                { order: 3, ingredient: sugar, amountDevided: 1 }
            ],
            equipment: Equipment.bowlNormal2,
            use: 'mep4',
            as: Alias.sauce
        },
        {
            id: 'cook7',
            type: InstructionType.cook,
            actions: [ { timerAt: 0, action: Action.layers } ],
            ingredients: [
                { order: 1, use: 'cook6', amountDevided: 0.3 },
                { order: 2, use: 'cook3', amountDevided: 0.5 },
                { order: 3, use: 'mep3', amountDevided: 0.5 },
                { order: 4, use: 'cook6', amountDevided: 0.3 },
                { order: 5, use: 'cook3', amountDevided: 0.5 },
                { order: 6, use: 'mep3', amountDevided: 0.5 },
                { order: 7, use: 'cook6', amountDevided: 0.4 },
                // TODO: FIXME: used to be 'mep1' where action is rasp on old_cheese
                // { order: 8, use: rasped_old_cheese, amountDevided: 1 }
            ],
            use: 'prep2',
        },
        // bake .. 22 minutes.. assembled ovendish .. into a baked ovendish ..
        // {
        //     id: 'cook8',
        //     timerInSeconds: 22 * 60,
        //     actions: [ { timerAt: 0, action: Action.Bake } ],
        //     ingredients: [
        //         { order: 1, use: 'cook7', amountDevided: 1 }
        //     ],
        //     use: 'prep1',
        // },
        // layer full plate.. (with) ingredients.. into a dish
        // {
        //     id: 'cook9',
        //     actions: [
        //         { timerAt: 0, action: Action.LayerFull }
        //     ],
        //     ingredients: [
        //         { order: 1, use: 'cook4', amountDevided: 1 },
        //         { order: 2, use: 'cook8', amountDevided: 1 },
        //         { order: 3, use: 'mep5', amountDevided: 1 },
        //     ],
        //     equipment: Equipment.PlateNormal
        // }
    ],
    serve: [
        { timing: MealServeTiming.before, use: 'Spoon' },
        { timing: MealServeTiming.before, use: 'Fork' },
        { timing: MealServeTiming.during, use: 'cook9' },
        { timing: MealServeTiming.during, use: 'cook2' } // alias Salad
    ]
} as Meal;
