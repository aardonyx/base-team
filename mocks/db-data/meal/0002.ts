import {
    garlic,
    olive_oil,
    water,
    parsley,
    onion_yellow,
    pepper_red,
    pepper_bell_yellow,
    stock_cube_vegetable,
    egg,
    tomato_plum,
    goat_cheese,
    bun_beer_bostel,
} from '../ingredient/ingredients';

import {
    Meal,
} from '../../../types/__generated__/graphql';

import { getObjectId } from 'mongo-seeding';

// if an id like 'mep', 'prep' or 'cook' is referenced somewhere else with 'use',
// then that object should also have an 'as' to make a 'beautiful reference'.

export = {
    _id: getObjectId('meal0002'),
    title: {
        en: 'Shakshuka with fresh goatcheese',
        nl: 'Shakshuka met verse geitenkaas'
    },
    equipment: [],
    amounts: [
        // make sure each ingredient used, is also in this list.
        { ingredient: bun_beer_bostel, amountPerPerson: 10 },
        { ingredient: egg, amountPerPerson: 90 },
        { ingredient: garlic, amountPerPerson: 1 },
        { ingredient: goat_cheese, amountPerPerson: 1 },
        { ingredient: onion_yellow, amountPerPerson: 40 },
        { ingredient: olive_oil, amountPerPerson: 1 },
        { ingredient: parsley, amountPerPerson: 3 },
        { ingredient: pepper_bell_yellow, amountPerPerson: 0.5 },
        { ingredient: pepper_red, amountPerPerson: 1 },
        { ingredient: stock_cube_vegetable, amountPerPerson: 0.5 },
        { ingredient: tomato_plum, amountPerPerson: 150 },
        { ingredient: water, amountPerPerson: 250 },
    ],
    instructions: [],
    serve: []
} as Meal;
