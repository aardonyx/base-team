import { olive_oil_extra_virgin } from "../ingredient/ingredients";

export const user1 = {
    home: [
        { ingredient: olive_oil_extra_virgin, inStock: true }
    ]
}