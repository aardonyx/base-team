import * as pkg from './package.json';
import { Props as AuthProps } from './constructs/auth';

const pascalCaseProjectName = pkg.name
    .split('-')
    .map((part: string): string => part.charAt(0).toUpperCase() + part.slice(1))
    .join('');

const uuid = Buffer.from(pkg.name).toString('hex').substr(0, 5);
export const authConfig: AuthProps = {
    userPoolName: `${pascalCaseProjectName}UserPool`,
    authDomainPrefix: `${pkg.name}-${uuid}`,
    appClients: [
        // ENABLE FOR EACH AVAILABLE CLIENT
        // {
        //     appUrls: [
        //         'http://localhost:3000/',
        //         'https://admin.rtbprojects.com',
        //     ],
        //     cognitoClientName: `PlantyAdmin`,
        //     // Used to generate the Hosted UI URL Output
        //     allowedOAuthFlows: ['code'],
        //     allowedOAuthScopes: ['phone', 'email', 'openid', 'profile'],
        // },
    ],
    identityProviders: [
        // ENABLE WHEN NEEDED
        // eslint-disable @typescript-eslint/camelcase
        // {
        //     type: ProviderType.GOOGLE,
        //     client_id: '<UNIQUE_ID>.apps.googleusercontent.com',
        //     // As set in the Google Console
        //     authorize_scopes: 'openid email profile',
        // },
        // eslint-enable
    ],
};

export const apiConfig = {
    apiName: `${pascalCaseProjectName}Api`,
};
