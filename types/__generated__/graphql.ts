export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  AWSDateTime: any;
  AWSDate: any;
  AWSURL: any;
  ObjectId: any;
};


export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
};

export type Alpha = {
  __typename?: 'Alpha';
  id?: Maybe<Scalars['ID']>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type Meal = {
  __typename?: 'Meal';
  _id: Scalars['ObjectId'];
  title: Translation;
  equipment: Array<MealEquipment>;
  amounts: Array<MealAmounts>;
  instructions: Array<Instruction>;
  serve: Array<MealServe>;
};

export type MealAmounts = {
  __typename?: 'MealAmounts';
  ingredient: Ingredient;
  amountPerPerson: Scalars['Float'];
};

export type MealServe = {
  __typename?: 'MealServe';
  timing: MealServeTiming;
  use: Scalars['String'];
};

export type MealEquipment = {
  __typename?: 'MealEquipment';
  equipment: Equipment;
  as?: Maybe<Alias>;
};

export type Ingredient = {
  __typename?: 'Ingredient';
  _id: Scalars['ObjectId'];
  name: Translation;
  priceInEurocents?: Maybe<Scalars['Float']>;
  unit: MeasurementUnit;
  home?: Maybe<Scalars['Boolean']>;
};

export type Translation = {
  __typename?: 'Translation';
  en: Scalars['String'];
  nl: Scalars['String'];
};

export type Instruction = {
  __typename?: 'Instruction';
  id: Scalars['String'];
  order?: Maybe<Scalars['Int']>;
  type?: Maybe<InstructionType>;
  timerInSeconds?: Maybe<Scalars['Float']>;
  actions: Array<InstructionActions>;
  ingredients?: Maybe<Array<Maybe<InstructionIngredients>>>;
  equipment?: Maybe<Equipment>;
  use?: Maybe<Scalars['String']>;
  as?: Maybe<Alias>;
};

export type InstructionActions = {
  __typename?: 'InstructionActions';
  timerAt: Scalars['Float'];
  action: Action;
};

export type InstructionIngredients = {
  __typename?: 'InstructionIngredients';
  order: Scalars['Float'];
  ingredient?: Maybe<Ingredient>;
  use?: Maybe<Scalars['String']>;
  amountDevided: Scalars['Float'];
};

export enum MeasurementUnit {
  piece = 'piece',
  tableSpoon = 'tableSpoon',
  teaSpoon = 'teaSpoon',
  gram = 'gram',
  milliliter = 'milliliter',
  leaves = 'leaves',
  clove = 'clove'
}

export enum Action {
  rinse = 'rinse',
  cutCubesSmall = 'cutCubesSmall',
  cutCubesNormal = 'cutCubesNormal',
  cutCubesLarge = 'cutCubesLarge',
  cutQuarts = 'cutQuarts',
  cutFine = 'cutFine',
  cutSlicesThin = 'cutSlicesThin',
  cutSlicesNormal = 'cutSlicesNormal',
  cutSlicesThick = 'cutSlicesThick',
  rasp = 'rasp',
  press = 'press',
  tear = 'tear',
  heat_220_celcius = 'heat_220_celcius',
  bake = 'bake',
  grease = 'grease',
  sprinkleBothSides = 'sprinkleBothSides',
  frySoft = 'frySoft',
  fryNormal = 'fryNormal',
  fryHard = 'fryHard',
  boil = 'boil',
  cook = 'cook',
  drain = 'drain',
  steamOut = 'steamOut',
  flip = 'flip',
  mix = 'mix',
  flavorPeperAndSalt = 'flavorPeperAndSalt',
  flavorPeper = 'flavorPeper',
  flavorSalt = 'flavorSalt',
  layer = 'layer',
  layers = 'layers',
  layerLeft = 'layerLeft',
  layerRight = 'layerRight'
}

export enum Equipment {
  cutboard = 'cutboard',
  oven = 'oven',
  ovenDish = 'ovenDish',
  fryingPan = 'fryingPan',
  cookingPanWithCover = 'cookingPanWithCover',
  bowlSmall = 'bowlSmall',
  bowlNormal1 = 'bowlNormal1',
  bowlNormal2 = 'bowlNormal2',
  bowlLarge = 'bowlLarge',
  plateNormal = 'plateNormal',
  tableSpoon = 'tableSpoon',
  tableFork = 'tableFork'
}

export enum Alias {
  pastaPan = 'pastaPan',
  saladBowl = 'saladBowl',
  sauceBowl = 'sauceBowl',
  dressing = 'dressing',
  salad = 'salad',
  garnish = 'garnish',
  sauce = 'sauce',
  preparedOvenDish = 'preparedOvenDish'
}

export enum InstructionType {
  cut = 'cut',
  prepare = 'prepare',
  cook = 'cook'
}

export enum MealServeTiming {
  before = 'before',
  during = 'during',
  after = 'after'
}

export type Query = {
  __typename?: 'Query';
  _noop?: Maybe<Scalars['String']>;
  getMeal?: Maybe<Meal>;
  listMeals?: Maybe<MealConnection>;
  getUser?: Maybe<User>;
  listUsers?: Maybe<UserConnection>;
  getAlpha?: Maybe<Alpha>;
  listAlphas?: Maybe<AlphaConnection>;
};


export type QuerygetMealArgs = {
  id: Scalars['ID'];
};


export type QuerylistMealsArgs = {
  filter?: Maybe<MealFilterInput>;
  limit?: Maybe<Scalars['Int']>;
  nextToken?: Maybe<Scalars['String']>;
};


export type QuerygetUserArgs = {
  id: Scalars['ID'];
};


export type QuerylistUsersArgs = {
  filter?: Maybe<UserFilterInput>;
  limit?: Maybe<Scalars['Int']>;
  nextToken?: Maybe<Scalars['String']>;
};


export type QuerygetAlphaArgs = {
  id: Scalars['ID'];
};


export type QuerylistAlphasArgs = {
  filter?: Maybe<AlphaFilterInput>;
  limit?: Maybe<Scalars['Int']>;
  nextToken?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  _noop?: Maybe<Scalars['String']>;
  createMeal?: Maybe<Meal>;
  updateMeal?: Maybe<Meal>;
  deleteMeal?: Maybe<Meal>;
  createUser?: Maybe<User>;
  updateUser?: Maybe<User>;
  deleteUser?: Maybe<User>;
  createAlpha?: Maybe<Alpha>;
  updateAlpha?: Maybe<Alpha>;
  deleteAlpha?: Maybe<Alpha>;
};


export type MutationcreateMealArgs = {
  input: MealCreateInput;
  condition?: Maybe<MealConditionInput>;
};


export type MutationupdateMealArgs = {
  input: MealUpdateInput;
  condition?: Maybe<MealConditionInput>;
};


export type MutationdeleteMealArgs = {
  input: MealDeleteInput;
  condition?: Maybe<MealConditionInput>;
};


export type MutationcreateUserArgs = {
  input: UserCreateInput;
  condition?: Maybe<UserConditionInput>;
};


export type MutationupdateUserArgs = {
  input: UserUpdateInput;
  condition?: Maybe<UserConditionInput>;
};


export type MutationdeleteUserArgs = {
  input: UserDeleteInput;
  condition?: Maybe<UserConditionInput>;
};


export type MutationcreateAlphaArgs = {
  input: AlphaCreateInput;
  condition?: Maybe<AlphaConditionInput>;
};


export type MutationupdateAlphaArgs = {
  input: AlphaUpdateInput;
  condition?: Maybe<AlphaConditionInput>;
};


export type MutationdeleteAlphaArgs = {
  input: AlphaDeleteInput;
  condition?: Maybe<AlphaConditionInput>;
};

export type Subscription = {
  __typename?: 'Subscription';
  _noop?: Maybe<Scalars['String']>;
  onCreateMeal?: Maybe<Meal>;
  onUpdateMeal?: Maybe<Meal>;
  onDeleteMeal?: Maybe<Meal>;
  onCreateUser?: Maybe<User>;
  onUpdateUser?: Maybe<User>;
  onDeleteUser?: Maybe<User>;
  onCreateAlpha?: Maybe<Alpha>;
  onUpdateAlpha?: Maybe<Alpha>;
  onDeleteAlpha?: Maybe<Alpha>;
};




export type S3Object = {
  __typename?: 'S3Object';
  bucket: Scalars['String'];
  region: Scalars['String'];
  key: Scalars['String'];
};


export enum AttributeTypes {
  binary = 'binary',
  binarySet = 'binarySet',
  bool = 'bool',
  list = 'list',
  map = 'map',
  number = 'number',
  numberSet = 'numberSet',
  string = 'string',
  stringSet = 'stringSet',
  _null = '_null'
}

export enum SortDirection {
  ASC = 'ASC',
  DESC = 'DESC'
}

export type StringInput = {
  ne?: Maybe<Scalars['String']>;
  eq?: Maybe<Scalars['String']>;
  le?: Maybe<Scalars['String']>;
  lt?: Maybe<Scalars['String']>;
  ge?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  notContains?: Maybe<Scalars['String']>;
  between?: Maybe<Array<Maybe<Scalars['String']>>>;
  beginsWith?: Maybe<Scalars['String']>;
  attributeExists?: Maybe<Scalars['Boolean']>;
  attributeType?: Maybe<AttributeTypes>;
  size?: Maybe<SizeInput>;
};

export type IDInput = {
  ne?: Maybe<Scalars['ID']>;
  eq?: Maybe<Scalars['ID']>;
  le?: Maybe<Scalars['ID']>;
  lt?: Maybe<Scalars['ID']>;
  ge?: Maybe<Scalars['ID']>;
  gt?: Maybe<Scalars['ID']>;
  contains?: Maybe<Scalars['ID']>;
  notContains?: Maybe<Scalars['ID']>;
  between?: Maybe<Array<Maybe<Scalars['ID']>>>;
  beginsWith?: Maybe<Scalars['ID']>;
  attributeExists?: Maybe<Scalars['Boolean']>;
  attributeType?: Maybe<AttributeTypes>;
  size?: Maybe<SizeInput>;
};

export type IntInput = {
  ne?: Maybe<Scalars['Int']>;
  eq?: Maybe<Scalars['Int']>;
  le?: Maybe<Scalars['Int']>;
  lt?: Maybe<Scalars['Int']>;
  ge?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  between?: Maybe<Array<Maybe<Scalars['Int']>>>;
  attributeExists?: Maybe<Scalars['Boolean']>;
  attributeType?: Maybe<AttributeTypes>;
};

export type FloatInput = {
  ne?: Maybe<Scalars['Float']>;
  eq?: Maybe<Scalars['Float']>;
  le?: Maybe<Scalars['Float']>;
  lt?: Maybe<Scalars['Float']>;
  ge?: Maybe<Scalars['Float']>;
  gt?: Maybe<Scalars['Float']>;
  between?: Maybe<Array<Maybe<Scalars['Float']>>>;
  attributeExists?: Maybe<Scalars['Boolean']>;
  attributeType?: Maybe<AttributeTypes>;
};

export type BooleanInput = {
  ne?: Maybe<Scalars['Boolean']>;
  eq?: Maybe<Scalars['Boolean']>;
  attributeExists?: Maybe<Scalars['Boolean']>;
  attributeType?: Maybe<AttributeTypes>;
};

export type SizeInput = {
  ne?: Maybe<Scalars['Int']>;
  eq?: Maybe<Scalars['Int']>;
  le?: Maybe<Scalars['Int']>;
  lt?: Maybe<Scalars['Int']>;
  ge?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  between?: Maybe<Array<Maybe<Scalars['Int']>>>;
};

export type StringKeyConditionInput = {
  eq?: Maybe<Scalars['String']>;
  le?: Maybe<Scalars['String']>;
  lt?: Maybe<Scalars['String']>;
  ge?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  between?: Maybe<Array<Maybe<Scalars['String']>>>;
  beginsWith?: Maybe<Scalars['String']>;
};

export type IDKeyConditionInput = {
  eq?: Maybe<Scalars['ID']>;
  le?: Maybe<Scalars['ID']>;
  lt?: Maybe<Scalars['ID']>;
  ge?: Maybe<Scalars['ID']>;
  gt?: Maybe<Scalars['ID']>;
  between?: Maybe<Array<Maybe<Scalars['ID']>>>;
  beginsWith?: Maybe<Scalars['ID']>;
};

export type IntKeyConditionInput = {
  eq?: Maybe<Scalars['Int']>;
  le?: Maybe<Scalars['Int']>;
  lt?: Maybe<Scalars['Int']>;
  ge?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  between?: Maybe<Array<Maybe<Scalars['Int']>>>;
};

export enum LambdaResult {
  success = 'success',
  error = 'error'
}

export type MealFilterInput = {
  _id?: Maybe<IDInput>;
  and?: Maybe<Array<Maybe<MealFilterInput>>>;
  or?: Maybe<Array<Maybe<MealFilterInput>>>;
  not?: Maybe<MealFilterInput>;
};

export type MealConditionInput = {
  _id?: Maybe<IDInput>;
  and?: Maybe<Array<Maybe<MealConditionInput>>>;
  or?: Maybe<Array<Maybe<MealConditionInput>>>;
  not?: Maybe<MealConditionInput>;
};

export type MealCreateInput = {
  _id?: Maybe<IDInput>;
};

export type MealUpdateInput = {
  _id?: Maybe<IDInput>;
};

export type MealDeleteInput = {
  _id?: Maybe<IDInput>;
};

export type MealConnection = {
  __typename?: 'MealConnection';
  items?: Maybe<Array<Maybe<Meal>>>;
  nextToken?: Maybe<Scalars['String']>;
};

export type UserFilterInput = {
  id?: Maybe<IDInput>;
  name?: Maybe<StringInput>;
  email?: Maybe<StringInput>;
  and?: Maybe<Array<Maybe<UserFilterInput>>>;
  or?: Maybe<Array<Maybe<UserFilterInput>>>;
  not?: Maybe<UserFilterInput>;
};

export type UserConditionInput = {
  title?: Maybe<StringInput>;
  name?: Maybe<StringInput>;
  email?: Maybe<StringInput>;
  and?: Maybe<Array<Maybe<UserConditionInput>>>;
  or?: Maybe<Array<Maybe<UserConditionInput>>>;
  not?: Maybe<UserConditionInput>;
};

export type UserCreateInput = {
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  email?: Maybe<StringInput>;
};

export type UserUpdateInput = {
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  email?: Maybe<StringInput>;
};

export type UserDeleteInput = {
  id?: Maybe<Scalars['ID']>;
};

export type UserConnection = {
  __typename?: 'UserConnection';
  items?: Maybe<Array<Maybe<User>>>;
  nextToken?: Maybe<Scalars['String']>;
};

export type AlphaConnection = {
  __typename?: 'AlphaConnection';
  items?: Maybe<Array<Maybe<Alpha>>>;
  nextToken?: Maybe<Scalars['String']>;
};

export type AlphaFilterInput = {
  id?: Maybe<IDInput>;
  title?: Maybe<StringInput>;
  description?: Maybe<StringInput>;
  status?: Maybe<StringInput>;
  and?: Maybe<Array<Maybe<AlphaFilterInput>>>;
  or?: Maybe<Array<Maybe<AlphaFilterInput>>>;
  not?: Maybe<AlphaFilterInput>;
};

export type AlphaConditionInput = {
  title?: Maybe<StringInput>;
  description?: Maybe<StringInput>;
  status?: Maybe<StringInput>;
  and?: Maybe<Array<Maybe<AlphaConditionInput>>>;
  or?: Maybe<Array<Maybe<AlphaConditionInput>>>;
  not?: Maybe<AlphaConditionInput>;
};

export type AlphaCreateInput = {
  id?: Maybe<Scalars['ID']>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type AlphaUpdateInput = {
  id: Scalars['ID'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
};

export type AlphaDeleteInput = {
  id?: Maybe<Scalars['ID']>;
};
